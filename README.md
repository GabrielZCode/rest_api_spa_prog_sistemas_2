Projeto de Programação de Sistemas 2 

Rest API com persistência de Dados de Classes declaradas no Diagrama de Classes de Domínio (Desenv de Sistemas 2)
SPA - Single Page Application para interação com a Rest API

Rest API desenvolvida em Spring Boot Kotlin, permitido pelo professor.

Gabriel Martins de Souza - Rest API - 32156154
Gustavo Macedo Rodrigues - Rest API - 42146321
Victória Alvez - SPA - 32198973
