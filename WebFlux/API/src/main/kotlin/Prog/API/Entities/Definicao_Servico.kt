package Prog.API.Entities

import javax.persistence.*
import javax.validation.constraints.*;
import java.util.Date

@Entity
@Table(name = "definicao_servico")
data class Definicao_Servico(
   @Column(unique=true, name ="id")
   @field:Id
   @field:GeneratedValue(strategy = GenerationType.IDENTITY)
    var id:Long,

    @field:NotNull
    @field:Size(min = 1, max =50)
    var numeroModelo: Int,

    @field:NotNull
    var precoHora:Float,

    @field:NotNull
    var descricao_servico:String,

    @field:NotNull
    var nome: String
){

      }
