package Prog.API.Entities

import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

data class Contrato_DTO(
    @field:NotEmpty(message = "O tipo de servico não pode estar em branco")
    val tipoServico: String,
    @field:NotNull
    val definicaoServico: Long,
    @field:NotNull
    val reserva: Long,
    val contratosRelacionados: List<Long>?,
    @field:NotEmpty(message = "A data de início não pode estar em branco")
    val dataInicio: String,
    @field:NotEmpty(message = "A data de fim não pode estar em branco")
    val dataFim: String,
    @field:NotNull
    val idCliente: Long,
    @field:NotNull
    val idAcompanhante: Long

)
