package Prog.API.Entities

import java.util.*
import javax.persistence.*
import javax.validation.constraints.*;

@Entity
data class Contrato(
    @Column(unique=true, name ="id")
   @field:Id
   @field:GeneratedValue(strategy = GenerationType.IDENTITY)
    var id:Long? = null,

    @field:NotNull
    @field:Size(min = 1, max =50)
    var tipoDeServico: String,

    @field:NotNull
    @OneToOne
    @JoinColumn(name="id")
    var definicao_servico: Definicao_Servico,

    @field:NotNull
    @OneToOne
    @JoinColumn(name="id")
    var reserva: Reserva,

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name="id")
    var contratosRelacionados: List<Contrato> = ArrayList(),

    @field:NotNull
    var dataInicio:String,

    @field:NotNull
    var dataFim:String,


    @field:NotNull
    var idCliente:Long,

    @field:NotNull
    var idAcompanhante:Long,
){

      }
