package Prog.API.Entities

import Prog.API.Entities.Definicao_Servico
import javax.persistence.*
import javax.validation.constraints.*;
import java.util.Date

@Entity
@Table(name = "reserva")
data class Reserva(

   @Column(unique=true, name ="id")
   @field:Id
   @field:GeneratedValue(strategy = GenerationType.IDENTITY)
    var id:Long,

    @field:NotNull
    var confirmacao:Boolean,

    @field:NotNull
    var nome: String,



    @field:NotNull
    @field:Size(min = 1, max =50)
    @field:Temporal(TemporalType.DATE)
    var dataInicio:Date,

    @field:NotNull
    @field:Size(min = 1, max =50)
    @field:Temporal(TemporalType.DATE)
    var dataFim:Date,
){

      }
