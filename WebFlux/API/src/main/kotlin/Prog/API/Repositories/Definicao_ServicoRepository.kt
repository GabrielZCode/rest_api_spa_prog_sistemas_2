
package Prog.API.Repositories

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import Prog.API.Entities.Definicao_Servico
import org.springframework.stereotype.Repository
import javax.persistence.Query
import javax.persistence.EntityManager

@Repository
interface Definicao_ServicoRepository :  JpaRepository<Definicao_Servico, Long>{
  }
