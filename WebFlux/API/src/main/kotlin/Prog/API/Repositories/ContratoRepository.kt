
package Prog.API.Repositories

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import Prog.API.Entities.Contrato
import org.springframework.stereotype.Repository
import javax.persistence.Query
import javax.persistence.EntityManager

@Repository
interface ContratoRepository :  JpaRepository<Contrato, Long>{
  }
