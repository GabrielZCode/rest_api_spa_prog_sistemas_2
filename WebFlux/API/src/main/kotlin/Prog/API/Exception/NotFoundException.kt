package Prog.API.Exception

import java.lang.RuntimeException

class NotFoundException(message: String?) : RuntimeException(message) {
}