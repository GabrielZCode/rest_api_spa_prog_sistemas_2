
package Prog.API.Controllers

import org.springframework.web.bind.annotation.RestController
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.beans.factory.annotation.Autowired
import org.hibernate.NonUniqueResultException
import javax.persistence.Query
import javax.persistence.EntityManager
import Prog.API.Repositories.ReservaRepository
import Prog.API.Entities.Reserva
import javax.persistence.EntityNotFoundException
import com.sun.jdi.request.DuplicateRequestException
import java.util.regex.Pattern
import javax.validation.Valid


@RestController
@RequestMapping("/reserva")
class ReservaController(
  val em: EntityManager
){

    @Autowired
    lateinit var repository: ReservaRepository

    @GetMapping
    fun returnAll(): List<Reserva> {
          return repository.findAll()
      }

    @PostMapping
    fun insert(@Valid @RequestBody reserva: Reserva): Reserva{
       return repository.save(reserva)
      }

    @PostMapping("/update/{id}")
    fun Update(@Valid @PathVariable("id") id:Long, @Valid @RequestBody reserva: Reserva ): Reserva{
        var tmp = repository.findById(id).orElseThrow{ EntityNotFoundException()}

       if(tmp == reserva){
           return reserva
         }
        if( tmp !== reserva){
            tmp.apply{
              this.confirmacao = reserva.confirmacao
              this.nome = reserva.nome
              this.dataInicio = reserva.dataInicio
              this.dataFim = reserva.dataFim
              }
              repository.save(tmp)
            return tmp
          }
      return tmp 
      }
  @PostMapping("/{id}")
  fun delete(@Valid @PathVariable("id") id:Long): Boolean{
    val reserva = repository.findById(id).orElseThrow{ EntityNotFoundException()}
    repository.delete(reserva);
    return true
    }
}
