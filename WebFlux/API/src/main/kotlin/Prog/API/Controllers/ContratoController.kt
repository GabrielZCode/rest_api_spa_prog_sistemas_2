
package Prog.API.Controllers

import org.springframework.web.bind.annotation.RestController
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.beans.factory.annotation.Autowired
import javax.persistence.EntityManager
import Prog.API.Repositories.ContratoRepository
import Prog.API.Entities.Contrato
import Prog.API.Entities.Contrato_DTO
import Prog.API.Mapper.Definicao_Contrato_Mapper
import javax.persistence.EntityNotFoundException
import javax.validation.Valid

@RestController
@RequestMapping("/contratos")
class ContratoController(
  val em: EntityManager,
  val contratoMapper: Definicao_Contrato_Mapper
){

    @Autowired
    lateinit var repository: ContratoRepository

    @GetMapping
    fun returnAll(): List<Contrato> {
          return repository.findAll()
      }

    @PostMapping
    fun insert(@Valid @RequestBody contratoDto: Contrato_DTO): Contrato{
        val contrato = contratoMapper.map(contratoDto)
       //Checar reserva e Definição no Banco de Dados com em.createNativeQuery ou funções de outros controlladores
       return repository.save(contrato)
      }

    @PostMapping("/update/{id}")
    fun Update(@Valid @PathVariable("id") id:Long, @Valid @RequestBody contrato: Contrato ): Contrato{
        var tmp = repository.findById(id).orElseThrow{ EntityNotFoundException()}

       if(tmp == contrato){
           return contrato
         }
        if( tmp !== contrato){
            tmp.apply{
                this.tipoDeServico = contrato.tipoDeServico
                this.reserva = contrato.reserva
                this.contratosRelacionados = contrato.contratosRelacionados
                this.dataInicio = contrato.dataInicio
                this.dataFim = contrato.dataFim
                this.idCliente = contrato.idCliente
                this.idAcompanhante = contrato.idAcompanhante
              }
              repository.save(tmp)
            return tmp
          }
      return tmp 
      }
  @PostMapping("/{id}")
  fun delete(@Valid @PathVariable("id") id:Long): Boolean{
    val contrato = repository.findById(id).orElseThrow{ EntityNotFoundException()}
    repository.delete(contrato);
    return true
    }
// Inserir contrato, necessitando de reserva e Definição Existentes
// Inserir contrato relacionado
}
