
package Prog.API.Controllers

import org.springframework.web.bind.annotation.RestController
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.beans.factory.annotation.Autowired
import org.hibernate.NonUniqueResultException
import javax.persistence.Query
import javax.persistence.EntityManager
import Prog.API.Repositories.Definicao_ServicoRepository
import Prog.API.Entities.Definicao_Servico
import javax.persistence.EntityNotFoundException
import com.sun.jdi.request.DuplicateRequestException
import java.util.regex.Pattern
import javax.validation.Valid

@RestController
@RequestMapping("/definicao")
class Definicao_ServicoController(
  val em: EntityManager
){

    @Autowired
    lateinit var repository: Definicao_ServicoRepository

    @GetMapping
    fun returnAll(): List<Definicao_Servico> {
          return repository.findAll()
      }
    @PostMapping
    fun insert(@Valid @RequestBody definicao: Definicao_Servico): Definicao_Servico{
       return repository.save(definicao)
      }

    @PostMapping("/update/{id}")
    fun Update(@Valid @PathVariable("id") id:Long, @Valid @RequestBody definicao: Definicao_Servico ): Definicao_Servico{
        var tmp = repository.findById(id).orElseThrow{ EntityNotFoundException()}

       if(tmp == definicao){
           return definicao
         }
        if( tmp !== definicao){
            tmp.apply{
              this.numeroModelo = definicao.numeroModelo
              this.precoHora = definicao.precoHora
              this.descricao_servico = definicao.descricao_servico
              this.nome = definicao.nome
              }
              repository.save(tmp)
            return tmp
          }
      return tmp 
      }
  @PostMapping("/{id}")
  fun delete(@Valid @PathVariable("id") id:Long): Boolean{
    val definicao = repository.findById(id).orElseThrow{ EntityNotFoundException()}
    repository.delete(definicao);
    return true
    }
}
