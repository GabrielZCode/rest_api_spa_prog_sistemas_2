package Prog.API.Mapper

import Prog.API.Entities.Contrato
import Prog.API.Entities.Contrato_DTO
import Prog.API.Exception.NotFoundException
import Prog.API.Repositories.Definicao_ServicoRepository
import Prog.API.Repositories.ReservaRepository
import org.springframework.stereotype.Component

@Component
class Definicao_Contrato_Mapper (
    val repositoryServico: Definicao_ServicoRepository,
    val repositoryReserva: ReservaRepository
        ) : Mapper<Contrato_DTO, Contrato>{
    override fun map(c: Contrato_DTO): Contrato {
        return Contrato(
            tipoDeServico = c.tipoServico,
            definicao_servico = repositoryServico.findById(c.definicaoServico).orElseThrow{NotFoundException("Serviço não encontrado")},
            reserva = repositoryReserva.findById(c.reserva).orElseThrow{NotFoundException("Serviço não encontrado")},
            dataInicio = c.dataInicio,
            dataFim = c.dataFim,
            idCliente = c.idCliente,
            idAcompanhante = c.idAcompanhante
        )
    }

}