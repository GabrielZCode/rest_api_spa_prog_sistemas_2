package Prog.API.Mapper

interface Mapper<C, U> {
    fun map(c: C): U
}